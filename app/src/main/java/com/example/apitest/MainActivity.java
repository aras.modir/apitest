package com.example.apitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text);

        API.getMovie().create(WebServices.class).getIP().enqueue(new Callback<IPPojo>() {
            @Override
            public void onResponse(Call<IPPojo> call, Response<IPPojo> response) {
                Log.d("TESTAPI", response.body().getCountry());
                IPPojo model = response.body();
                textView.setText(model.getCity());
            }

            @Override
            public void onFailure(Call<IPPojo> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error: " + t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
