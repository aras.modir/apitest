package com.example.apitest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WebServices {
    @GET("json")
    Call<IPPojo>
    getIP();

}
